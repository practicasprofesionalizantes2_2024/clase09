<?php
// Realizado para solicitar a un endpoint con autorización basica
// Endpoint de la API - Deben reemplazar por la ip que se le sea asignada
$api_endpoint = "http://10.0.20.20:5010/v2/dni";
$username="admin";
$password="admin12";
// Datos del formulario - recupero 
$dni = $_POST['dni'];
// Crear la url completa con los datos del pedido,en una solicitud sin restricción
$url_con_parametros = $api_endpoint. "?value=".trim($dni);
// Inicializar cURL
$ch = curl_init();
// Establecer la URL
curl_setopt($ch, CURLOPT_URL, $url_con_parametros);
// Establecer el método HTTP (GET en este caso , recuerden que puede variar)
curl_setopt($ch, CURLOPT_HTTPGET, true);
// Indicar que se quiere recibir la respuesta
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json',
    'Authorization: Basic ' . base64_encode("$username:$password")
]);
// Ejecutar la solicitud y obtener la respuesta
$response = curl_exec($ch);
$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
// Verificar si hubo algún error
if ($response === false) {
    echo "Error al llamar a la API: " . curl_error($ch);
} else {
// verifico que sea una respuesta valida
    if ($httpCode ==200)
    {
// Procesar la respuesta y diferenes maneras de mostrarlo
// decodifica el json en la varible data
        $data = json_decode($response, true);
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                echo "Key: $key\n";
                print_r($value);  
            } else {
                echo "key: $key -> valor: $value\n"."<br>";
            }
        }
    }
    else{
        echo "Solicitud incorrecta (400 Bad Request). Respuesta de la API:<br><pre>$response</pre>";
    }
}
// Cerrar cURL
curl_close($ch);
?>
