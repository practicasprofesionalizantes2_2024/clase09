<?php
// Realizado para solicitar a un endpoint con autorización basica
// Endpoint de la API - Deben reemplazar por la ip que se le sea asignada
$api_token_route="http://10.0.20.20:5010/login";
$api_endpoint = "http://10.0.20.20:5010/v3/dni";
$postData=json_encode(
    [
        'username' => "admin",
        'password' => "admin123"
    ]
    );
// Datos del formulario - recupero 
$dni = $_POST['dni'];
/// 1er pegada para obtener Token
$ch = curl_init();
// Establecer la URL
curl_setopt($ch, CURLOPT_URL, $api_token_route);
// Establecer el método HTTP (GET en este caso , recuerden que puede variar)
curl_setopt($ch, CURLOPT_HTTPGET, true);
// Indicar que se quiere recibir la respuesta
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json'
]);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
// Ejecutar la solicitud y obtener la respuesta
$response = curl_exec($ch);
$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
// Verificar si hubo algún error
if ($response === false) {
    echo "Error al llamar a la API para obtener Token de Seguridad: " . curl_error($ch);
} 
else {
// verifico que sea una respuesta valida
    if ($httpCode ==200)
    {
// decodifica el json en la varible data
        $data = json_decode($response, true);
        foreach ($data as $key => $value) {
            if (is_array($value)) {
//                echo "Key: $key\n";
//                print_r($value);  
            } else {
//                echo "key: $key -> valor: $value\n"."<br>";
                $token=$value;
                
            }
        }
    }
    else{
        echo "Solicitud incorrecta (400 Bad Request). Respuesta de la API:<br><pre>$response</pre>";
    }
}
curl_close($ch);
// Crear la url completa con los datos del pedido,en una solicitud sin restricción
$url_con_parametros = $api_endpoint. "?value=".trim($dni);
// Inicializar cURL
$ch = curl_init();
// Establecer la URL
curl_setopt($ch, CURLOPT_URL, $url_con_parametros);
// Establecer el método HTTP (GET en este caso , recuerden que puede variar)
curl_setopt($ch, CURLOPT_HTTPGET, true);
// Indicar que se quiere recibir la respuesta
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json',
    'Authorization: Bearer ' . $token
]);
// Ejecutar la solicitud y obtener la respuesta
$response = curl_exec($ch);
$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
// Verificar si hubo algún error
if ($response === false) {
    echo "Error al llamar a la API: " . curl_error($ch);
} else {
// verifico que sea una respuesta valida
    if ($httpCode ==200)
    {
// decodifica el json en la varible data
        $data = json_decode($response, true);
        $dni=base64_encode($data['dni']);
        $apellidos=$data['apellidos'];
        $nombres=$data['nombres'];
        $mesa=$data['mesa'];
       foreach ($data as $key => $value) {
            if (is_array($value)) {
                echo "Key: $key\n";
                print_r($value);  
            } else {
                echo "key: $key -> valor: $value\n"."<br>";
                
            }
            
        }
        echo "<a href='imprimir.php?dni=$dni&apellidos=$apellidos&nombres=$nombres&mesa=$mesa'.'target='_blank'>Imprimir Reporte</a>";
    }
    else{
        echo "Solicitud incorrecta (400 Bad Request). Respuesta de la API:<br><pre>$response</pre>";
    }
}
// Cerrar cURL
curl_close($ch);
?>