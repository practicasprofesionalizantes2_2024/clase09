<?php
require('fpdf/fpdf.php');
if (!isset($_GET['dni']) && !isset($_GET['apellidos']) && !isset($_GET['nombres'])) {
    echo "No se recibieron todos los parámetros necesarios.";
}
else{

    {
        class PDF extends FPDF
        {
            // Cabecera de página
            function Header()
            {
                // Logo
                $this->Image('logo.png',10,8,33); // Reemplaza 'logo.png' por la ruta de tu logo
                // Arial bold 15
                $this->SetFont('Arial','B',15);
                // Movernos a la derecha
                $this->Cell(80);
                // Título
                $this->Cell(30,10,'Reporte del Votante',0,0,'C');
                // Salto de línea
                $this->Ln(20);
            }

            // Pie de página
            function Footer()
            {
                // Posición a 1.5 cm del final
                $this->SetY(-15);
                // Arial italic 8
                $this->SetFont('Arial','I',8);
                // Número de página
                $this->Cell(0,10,'Página '.$this->PageNo().'/{nb}',0,0,'C');
            }
        }

        // Crear instancia de la clase heredada
        $pdf = new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,30,'DNI:'.base64_decode($_GET['dni']),0,1,'C');
        $pdf->Cell(10,35,'Apellidos:'.$_GET['apellidos'],0,1);
        $pdf->Cell(10,40,'Nombres:'.$_GET['nombres'],0,1);
        $pdf->Cell(10,45,'Mesa:'.$_GET['mesa'],0,1);
        $pdf->Output();
    }
}
?>
